<h1 align="center">三方组件资源汇总</h1> 

##### 本文收集了一些已经开源的三方组件资源，欢迎应用开发者参考和使用，同时也欢迎开发者贡献自己的开源组件库，可以提PR加入到列表当中


## 目录

- [工具](#工具)
- [三方组件JS_ETS](#三方组件JS_ETS)
    - [工具类](#工具类JS_ETS)
        - [图片加载](#图片加载JS_ETS)
        - [相机-相册](#相机-相册JS_ETS)
        - [数据封装传递](#数据封装传递JS_ETS)
        - [日志](#日志JS_ETS)
        - [蓝牙工具](#蓝牙工具JS_ETS)
        - [文件解析编码解码](#文件解析编码解码JS_ETS)
        - [算法类](#算法类JS_ETS)
        - [其他工具类](#其他工具类JS_ETS)
    - [网络类](#网络类-JS_ETS)
        - [网络类](#网络类JS_ETS)
    - [文件数据类](#文件数据类JS_ETS)
        - [数据库](#数据库JS_ETS)
        - [数据存储](#数据存储JS_ETS)
    - [UI-自定义控件](#ui-自定义控件JS_ETS)
        - [Image](#image-JS_ETS)
        - [Text](#text-JS_ETS)
        - [ListView](#listview-JS_ETS)
        - [Indicator](#indicator-JS_ETS)
        - [PageSlider](#pageslider-JS_ETS)
        - [ProgressBar](#progressbar-JS_ETS)
        - [Dialog](#dialog-JS_ETS)
        - [Layout](#layout-JS_ETS)
        - [Tab-菜单切换](#tab-菜单切换JS_ETS)
        - [选择器](#选择器JS_ETS)
        - [其他UI-自定义控件](#其他ui-自定义控件JS_ETS)
    - [动画图形类](#动画图形类JS_ETS)
        - [动画](#动画JS_ETS)
        - [图片处理](#图片处理JS_ETS)
    - [音视频](#音视频JS_ETS)
- [三方组件C_CPP](#三方组件C_CPP)
    - [工具类](#工具类C_CPP)
        - [音视频](#音视频C_CPP)
        - [加解密算法](#加解密算法C_CPP)
        - [图像处理](#图像处理C_CPP)
        - [网络通信](#网络通信C_CPP)
        - [其他工具类](#其他工具类C_CPP)


## 工具

- [IDE官方下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio) - DevEco Studio
- [HAPM官网地址](https://hpm.harmonyos.com/hapm/#/cn/home) - [HAPM介绍](https://hpm.harmonyos.com/hapm/#/cn/help/introduction)

[返回目录](#目录)

## <a name="三方组件JS_ETS"></a>三方组件JS_ETS

### <a name="工具类JS_ETS"></a>工具类

#### <a name="图片加载JS_ETS"></a>图片加载

- :tw-1f195: [ImageKnife](https://gitee.com/openharmony-tpc/ImageKnife) - 更高效、更轻便、更简单的图像加载缓存库，能力类似java组件glide、disklrucache、glide-transformations、fresco、picasso、uCrop、Luban、pngj、Android-Image-Cropper、android-crop等库
- :tw-1f195: [ImageViewZoom](https://gitee.com/openharmony-sig/ImageViewZoom) - 图片加载组件，支持缩放和平移

[返回目录](#目录)

#### <a name="相机-相册JS_ETS"></a>相机-相册

- :tw-1f195: [zxing](https://gitee.com/openharmony-tpc/zxing) - 一个解析/生成二维码的组件，能力类似java组件zxing，Zbar、zxing-android-embedded、BGAQRCode-Android等
- :tw-1f195: [qr-code-generator](https://gitee.com/openharmony-sig/qr-code-generator) - 二维码生成器

[返回目录](#目录)

#### <a name="数据封装传递JS_ETS"></a>数据封装传递

- :tw-1f195: [LiveEventBus-ETS](https://gitee.com/openharmony-sig/LiveEventBus-ETS) - 消息总线，支持Sticky，支持跨进程，支持跨应用广播
- :tw-1f195: [RxJS](https://gitee.com/openharmony-tpc/openharmony_tpc_samples) - 用于使用可观察序列和流畅的查询运算符组成异步，在openharmony上的sample
- :tw-1f195: [EventBus](https://gitee.com/openharmony-tpc/openharmony_tpc_samples) - 最常用的消息传递工具，发布/订阅事件总线，在openharmony上的sample

[返回目录](#目录)

#### <a name="日志JS_ETS"></a>日志

- :tw-1f195: [logback](https://gitee.com/hihopeorg/logback) - 日志组件，支持打印与保存，能力类似java组件logback，commons-logging，minlog，slf4j等库

[返回目录](#目录)

#### <a name="蓝牙工具JS_ETS"></a>蓝牙工具

- :tw-1f195: [FastBle-ETS](https://gitee.com/openharmony-sig/FastBle-ETS) - 蓝牙能力集成工具，支持过滤，扫描，链接，读取，写入
- :tw-1f195: [ohos-beacon-library](https://gitee.com/openharmony-sig/ohos-beacon-library) - 应用与蓝牙信标交互组件

[返回目录](#目录)

#### <a name="文件解析编码解码JS_ETS"></a>文件解析编码解码

- :tw-1f195: [protobuf](https://gitee.com/openharmony-tpc/protobuf) - 序列化和反序列化，能力类似java组件protobuf，libprotobuf-mutator等库
- :tw-1f195: [okio](https://gitee.com/openharmony-tpc/okio) - 一个通过数据流、序列化和文件系统为系统输入和输出提供支持的库
- :tw-1f195: [jtar](https://gitee.com/openharmony-sig/jtar) - 提供了一种使用IO流创建和读取 tar 文件方法的库
- :tw-1f195: [commonmark](https://gitee.com/openharmony-tpc/commonmark) - 高度可扩展的 Markdown 解析器
- :tw-1f195: [CommonsCompressEts](https://gitee.com/openharmony-tpc/CommonsCompressEts) - 压缩/解压功能组件，能力类似java组件common-compress，zip4j，aircompressor，7zip等库
- :tw-1f195: [commons-codec](https://gitee.com/openharmony-tpc/commons-codec) - 一个包含各种格式的简单编码器和解码器
- :tw-1f195: [juniversalchardet](https://gitee.com/openharmony-sig/juniversalchardet) - 字符编码识别组件
- :tw-1f195: [snakeyaml](https://gitee.com/openharmony-sig/snakeyaml) - YAML文件解析器
- :tw-1f195: [base64](https://gitee.com/openharmony-sig/base64) - base64编解码器
- :tw-1f195: [jsoup](https://gitee.com/openharmony-sig/jsoup) - HTML解析器，能力类似java组件jsoup，htmlcleaner，templa等库
- :tw-1f195: [commons-cli](https://gitee.com/openharmony-sig/commons-cli) - 该库用于解析传递给程序的命令行选项
- :tw-1f195: [brotli](https://gitee.com/openharmony-sig/brotli) - Brotli 是一种通用无损压缩算法
- :tw-1f195: [dd-plist](https://gitee.com/openharmony-sig/dd-plist) - plist文件解析库
- :tw-1f195: [avro](https://gitee.com/hihopeorg/avro) - Avro是一个数据序列化的系统，可以将数据结构或对象转化成便于存储或传输的格式，适合于远程或本地大规模数据的存储和交换

[返回目录](#目录)

#### <a name="算法类JS_ETS"></a>算法类
- :tw-1f195: [checksum](https://gitee.com/openharmony-sig/checksum) - 计算散列函数的组件，如sha1，MD5等
- :tw-1f195: [crypto-js](https://gitee.com/openharmony-sig/crypto-js) - 加密算法类库，目前支持MD5、SHA-1、SHA-256、HMAC、HMAC-MD5、HMAC-SHA1、HMAC-SHA256、PBKDF2等
- :tw-1f195: [jama-ets](https://gitee.com/openharmony-sig/jama-ets) - 基本线性代数包，它提供了用于构造和操作真实密集矩阵的用户级类，各种构造函数从双精度浮点数的二维数组创建矩阵
- :tw-1f195: [jchardet](https://gitee.com/openharmony-sig/jchardet) - 自动字符集检测算法
- :tw-1f195: [is-png](https://gitee.com/openharmony-sig/is-png) - 判断是否是png格式文件的库
- :tw-1f195: [is-webp](https://gitee.com/openharmony-sig/is-webp) - 判断是否是webp的库

[返回目录](#目录)

#### <a name="其他工具类JS_ETS"></a>其他工具类
- :tw-1f195: [VCard](https://gitee.com/openharmony-tpc/VCard) - 电子名片的文件格式标准
- :tw-1f195: [flexsearch-ohos](https://gitee.com/openharmony-tpc/flexsearch-ohos) - 最快且最具内存灵活性的全文搜索库
- :tw-1f195: [thrift](https://gitee.com/openharmony-tpc/thrift) - 一个轻量级的、独立于语言的软件堆栈，用于点对点RPC实现
- :tw-1f195: [pinyin4js](https://gitee.com/openharmony-tpc/pinyin4js) - 一个功能强大的拼音库，能力类似java组件pinyin4j，TinyPinyin等库。
- :tw-1f195: [arouter-api-onActivityResult](https://gitee.com/openharmony-tpc/arouter-api-onActivityResult) - 用于在各种应用或页面间的跳转和页面间的数据传递
- :tw-1f195: [mixpanel-ohos](https://gitee.com/openharmony-tpc/mixpanel-ohos) - 一种可捕获有关用户如何与数字产品交互的数据产品分析工具
- :tw-1f195: [xutils](https://gitee.com/openharmony-sig/xutils) - 网络、文件、数据库操作工具集
- :tw-1f195: [Hamcrest](https://gitee.com/openharmony-sig/Hamcrest) - 单元测试框架
- :tw-1f195: [jmustache](https://gitee.com/openharmony-sig/jmustache) - Mustache 模板语言的js实现
- :tw-1f195: [libphonenumber](https://gitee.com/openharmony-tpc/openharmony_tpc_samples) - Google 用于解析、格式化和验证国际电话号码的通用 Java、C++ 和 JavaScript 库，在openharmony上的sample。
- :tw-1f195: [aws-sdk-js](https://gitee.com/openharmony-tpc/openharmony_tpc_samples) - 适用于 JavaScript的AWS 开发工具包，在openharmony上的sample。
- :tw-1f195: [mail](https://gitee.com/hihopeorg/mail) - 检测并解析MIME 格式的电子邮件消息流， 并构建电子邮件消息的组件合集。能力类似java组件jakarta-mail、mime4j、mime-types等库。

[返回目录](#目录)

### <a name="网络类-JS_ETS"></a>网络类

#### <a name="网络类JS_ETS"></a>网络类

- :tw-1f195: [httpclient](https://gitee.com/openharmony-tpc/httpclient) - 一个默认高效的 HTTP 客户端，能力类似java组件okhttp、legacy、chuck、android-async-http、httpclient、netty、AutobahnAndroid、OkGo等库的功能特性
- :tw-1f195: [retrofit](https://gitee.com/openharmony-tpc/retrofit) - 一款类型安全的 HTTP 客户端
- :tw-1f195: [okdownload](https://gitee.com/openharmony-sig/okdownload) - 文件下载工具
- :tw-1f195: [mars](https://gitee.com/hihopeorg/mars) - 跨平台网络组件
- :tw-1f195: [RocketChat](https://gitee.com/openharmony-tpc/RocketChat) - 服务器方法和消息流订阅的应用程序接口

[返回目录](#目录)

### <a name="文件数据类JS_ETS"></a>文件数据类

#### <a name="数据库JS_ETS"></a>数据库

- :tw-1f195: [greenDAO](https://gitee.com/openharmony-sig/greenDAO) - 数据库能力归一化组件，能力类似java组件greenDAO，DBFlow，android-database-sqlcipher，ormlit-core，ormlite-android等库

[返回目录](#目录)

#### <a name="数据存储JS_ETS"></a>数据存储

- :tw-1f195: [MMKV](https://gitee.com/openharmony-tpc/MMKV) - 一款小型键值对存储框架

[返回目录](#目录)

### <a name="ui-自定义控件JS_ETS"></a>UI-自定义控件

#### <a name="image-JS_ETS"></a>Image

- :tw-1f195: [subsampling-scale-image-view](https://gitee.com/openharmony-sig/subsampling-scale-image-view) - 视图缩放组件
- :tw-1f195: [RoundedImageView](https://gitee.com/openharmony-sig/RoundedImageView) - 圆角图片设置组件
- :tw-1f195: [CircleImageView](https://gitee.com/openharmony-sig/CircleImageView) - 自定义圆形imageview，主要实现圆形图片展示
- :tw-1f195: [PhotoView-ETS](https://gitee.com/openharmony-sig/PhotoView-ETS) - 图片加载、缩放、浏览组件
- [NineGridImageViewJS](https://gitee.com/openharmony-tpc/NineGridImageViewJS) - 九宫格图片展示
- [vant](https://gitee.com/vant-openharmony/vant) - 轻量、可靠的 openharmony UI 组件库，提供多种icon设计
- [OpenHarmony-JS-Icon](https://gitee.com/chenqiao002/open-harmony-js-icon) - 通过使用常用组件、画布组件和自定义组件等来实现一个自定义的icon组件
- [ContinuousScrollableImageJS](https://gitee.com/openharmony-tpc/ContinuousScrollableImageJS) - 带动画播放的Image

[返回目录](#目录)

#### <a name="text-JS_ETS"></a>Text

- :tw-1f195: [ohos-autofittextview](https://gitee.com/openharmony-sig/ohos-autofittextview) - 自适应边界的textview组件，可自动调整文本大小
- :tw-1f195: [DanmakuFlameMaster](https://gitee.com/openharmony-sig/DanmakuFlameMaster) - 弹幕放送、解析与绘制库

[返回目录](#目录)

#### <a name="listview-JS_ETS"></a>ListView

- :tw-1f195: [MultiType](https://gitee.com/openharmony-sig/MultiType) - 为list组件创建多种条目类型的库
- :tw-1f195: [recyclerview-animators-ets](https://gitee.com/openharmony-sig/recyclerview-animators-ets) - 带有添加删除动画效果以及整体动画效果的list组件库
- :tw-1f195: [overscroll-decor-ets](https://gitee.com/openharmony-sig/overscroll-decor-ets) - UI滚动组件

[返回目录](#目录)

#### <a name="indicator-JS_ETS"></a>Indicator

- :tw-1f195: [CircleIndicator](https://gitee.com/openharmony-sig/CircleIndicator) - 指示器归一化组件，能力类似java组件CircleIndicator，MagicIndicator，ViewPagerIndicator等库

[返回目录](#目录)

#### <a name="pageslider-JS_ETS"></a>PageSlider

- :tw-1f195: [RecyclerViewPager](https://gitee.com/openharmony-sig/RecyclerViewPager) - 支持无限循环、左右翻页切换效果、上下翻页切换效果、Material风格的容器
- [ohos-CoverflowJS](https://gitee.com/openharmony-tpc/ohos-CoverflowJS) - 轮播图自定义组件

[返回目录](#目录)

#### <a name="progressbar-JS_ETS"></a>ProgressBar

- :tw-1f195: [MaterialProgressBar](https://gitee.com/openharmony-sig/MaterialProgressBar) - 自定义进度条显示效果的归一化组件，能力类似java组件MaterialProgressBar，materialish-progress，SmoothProgressBar等库。
- [ArcProgressStackViewJS](https://gitee.com/openharmony-tpc/ArcProgressStackViewJS) - 弧形模式下显示进度条

[返回目录](#目录)

#### <a name="dialog-JS_ETS"></a>Dialog

- :tw-1f195: [material-dialogs](https://gitee.com/openharmony-sig/material-dialogs) - 自定义弹框组件
- [search_dialogJS](https://gitee.com/openharmony-tpc/search_dialogJS) - 搜索Dialog

[返回目录](#目录)

#### <a name="layout-JS_ETS"></a>Layout

- :tw-1f195: [vlayout](https://gitee.com/openharmony-sig/vlayout) - 布局扩展组件，提供一整套布局方案和布局间的组件复用功能
- :tw-1f195: [TextLayoutBuilder-ETS](https://gitee.com/openharmony-sig/TextLayoutBuilder-ETS) - 文本自定义布局组件
- :tw-1f195: [ohos-SwipeLayout](https://gitee.com/openharmony-sig/ohos-SwipeLayout) - 各种样式的滑动组件

[返回目录](#目录)

#### <a name="tab-菜单切换JS_ETS"></a>tab-菜单切换

- :tw-1f195: [navigation-bar](https://gitee.com/hihopeorg/navigation-bar) - 自定义导航栏组件

[返回目录](#目录)

#### <a name="选择器JS_ETS"></a>选择器

- :tw-1f195: [ohos-PickerView](https://gitee.com/openharmony-sig/ohos-PickerView) - 选择器，包括时间选择、地区选择、分割线设置、文字大小颜色设置
- :tw-1f195: [WheelPicker](https://gitee.com/openharmony-sig/WheelPicker) - 滚轮选择器

[返回目录](#目录)

#### <a name="其他ui-自定义控件JS_ETS"></a>其他ui-自定义控件

- :tw-1f195: [ohos-MPChart](https://gitee.com/openharmony-sig/ohos-MPChart) - 图表归一化组件，能力类似java组件AndroidMPChart，arhartengine等库。
- [RefreshLoadMoreComponentJS](https://gitee.com/openharmony-tpc/RefreshLoadMoreComponentJS) - 下拉刷新控件
- [SelectViewJS](https://gitee.com/openharmony-tpc/SelectViewJS) - 自定义选择组件，提供了本地查询和自动排序功能
- [StatusViewJS](https://gitee.com/openharmony-tpc/StatusViewJS) - 自定义不同状态组件
- [Image3DJs](https://gitee.com/openharmony-tpc/Image3DJs) - 根据监听手机传感器实现裸眼3D效果的控件
- [JsComponent](https://gitee.com/luzhen050/JsComponent) - 实现了水波纹、滚轮、浮动列表、表单等组件
- [Sheet](https://gitee.com/rekyone/sheet) - 基于 Canvas 实现的高性能 Excel 表格引擎组件 OpenHarmonySheet
- [OpenHarmany-Pretty-Weather](https://gitee.com/qiezisoftware/openharmany-pretty-weather) - 一个基于 OpenHarmony 下的 JavaScript 天气组件
- [CurtainJs](https://gitee.com/openharmony-tpc/CurtainJs) - 高亮显示局部区域
- [Neumorphism_Smarthome_Darkmode](https://github.com/applibgroup/Neumorphism_Smarthome_Darkmode) - Neumorphism library is an UI component which supports neumorphic effects
-  :tw-1f195: [Neumorphism_Smart_Watch](https://github.com/applibgroup/Neumorphism_Smart_Watch) - Neumorphism library is an UI component which supports neumorphic effects
-  :tw-1f195: [Neumorphism_Living-Room_Design](https://github.com/applibgroup/Neumorphism_Living-Room_Design) - Neumorphism library is an UI component which supports neumorphic effects
-  :tw-1f195: [Neumorphism_Smarthome_Lightmode](https://github.com/applibgroup/Neumorphism_Smarthome_Lightmode) - Neumorphism library is an UI component which supports neumorphic effects
-  :tw-1f195: [Alert](https://github.com/applibgroup/Alert) - Neumorphism library is an UI component which supports neumorphic effects
-  :tw-1f195: [Buttons](https://github.com/applibgroup/Buttons) - Neumorphism library is an UI component which supports neumorphic effects
-  :tw-1f195: [Card](https://github.com/applibgroup/Card) - Neumorphism library is an UI component which supports neumorphic effects
-  :tw-1f195: [Checkbox](https://github.com/applibgroup/Checkbox) - Neumorphism library is an UI component which supports neumorphic effects
-  :tw-1f195: [Dropdown](https://github.com/applibgroup/Dropdown) - Neumorphism library is an UI component which supports neumorphic effects
-  :tw-1f195: [form](https://github.com/applibgroup/form) - Neumorphism library is an UI component which supports neumorphic effects
-  :tw-1f195: [Navbar](https://github.com/applibgroup/Navbar) - Neumorphism library is an UI component which supports neumorphic effects
-  :tw-1f195: [pagination](https://github.com/applibgroup/pagination) - Neumorphism library is an UI component which supports neumorphic effects
-  :tw-1f195: [progress](https://github.com/applibgroup/progress) - Neumorphism library is an UI component which supports neumorphic effects
-  :tw-1f195: [Theme](https://github.com/applibgroup/Theme) - Neumorphism library is an UI component which supports neumorphic effects
-  :tw-1f195: [Verbal_Expressions](https://github.com/applibgroup/Verbal_Expressions) - Verbal Expressions is a Javascript library that helps construct difficult regular expressions.


[返回目录](#目录)


### <a name="动画图形类JS_ETS"></a>动画图形类

#### <a name="动画JS_ETS"></a>动画

- :tw-1f195: [lottieETS](https://gitee.com/openharmony-tpc/lottieETS) - 适用于OpenHarmony的动画库,功能类似于Java组件lottie，AndroidViewAnimations，Leonids等库。
- :tw-1f195: [shimmer-ohos](https://gitee.com/openharmony-sig/shimmer-ohos) - 提供各种形态的页面加载的闪烁效果
- :tw-1f195: [rebound](https://gitee.com/openharmony-tpc/rebound) - 用于模拟弹簧动力学以驱动物理动画库
- [LoadingViewJs](https://gitee.com/openharmony-tpc/LoadingViewJs) - 多种漂亮样式的加载动画

[返回目录](#目录)

#### <a name="图片处理JS_ETS"></a>图片处理

- :tw-1f195: [LargeImage](https://gitee.com/openharmony-tpc/LargeImage) - 加载可以执行缩放（放大和缩小）和滚动操作的图像
- :tw-1f195: [XmlGraphicsBatikETS](https://gitee.com/openharmony-tpc/XmlGraphicsBatikETS) - 用于处理可缩放矢量图形（SVG）格式的图像，例如显示、生成、解析或者操作图像
- :tw-1f195: [ohos-svg](https://gitee.com/openharmony-sig/ohos-svg) - SVG解析器
- :tw-1f195: [we-cropper](https://gitee.com/hihopeorg/we-cropper) - canvas图片裁剪器
- [JsImagePreview](https://gitee.com/thoseyears/js-image-preview) - 图片预览组件，包含水波纹动画、跳转动画以及相关手势

[返回目录](#目录)

### <a name="音视频JS_ETS"></a>音视频

- :tw-1f195: [mp4parser](https://gitee.com/openharmony-tpc/mp4parser) - 一个读取、写入操作音视频文件编辑的工具
- :tw-1f195: [mp3agic](https://gitee.com/openharmony-sig/mp3agic) - mp3文件ID3标签处理库

[返回目录](#目录)


## <a name="三方组件C_CPP"></a>三方组件C_CPP

### <a name="工具类C_CPP"></a>工具类

#### <a name="音视频C_CPP"></a>音视频
- :tw-1f195: [vorbis](https://gitee.com/openharmony-sig/vorbis) - 一种通用音频和音乐编码格式组件
- :tw-1f195: [opus](https://gitee.com/openharmony-sig/opus) - Opus是一个开放格式的有损声音编码格式
- :tw-1f195: [flac](https://gitee.com/openharmony-sig/flac) - 无损音频编解码器

[返回目录](#目录)

#### <a name="加解密算法C_CPP"></a>加解密算法
- :tw-1f195: [libogg](https://gitee.com/openharmony-sig/libogg) - 编解码器
- :tw-1f195: [libsodium](https://gitee.com/hihopeorg/libsodium) - 易用，可移植的加解密库

[返回目录](#目录)

#### <a name="图像处理C_CPP"></a>图像处理
- :tw-1f195: [stb-image](https://gitee.com/openharmony-sig/stb-image) - C/C++实现的图像解码库
- :tw-1f195: [pyclipper](https://gitee.com/openharmony-sig/pyclipper) - 图形处理库，可以用于解决平面二维图形的多边形简化、布尔运算和偏置处理

[返回目录](#目录)

#### <a name="网络通信C_CPP"></a>网络通信
- :tw-1f195: [nanopb](https://gitee.com/hihopeorg/nanopb) - 轻量的支持C语言的一种数据协议，可用于数据存储、通信协议等方面
- :tw-1f195: [c-ares](https://gitee.com/openharmony-sig/c-ares) - 异步解析器库，适用于需要无阻塞地执行 DNS 查询或需要并行执行多个 DNS 查询的应用程序
- :tw-1f195: [libevent](https://gitee.com/openharmony-sig/libevent) - 事件通知库
- :tw-1f195: [kcp](https://gitee.com/openharmony-sig/kcp) - ARQ 协议,可解决在网络拥堵情况下tcp协议的网络速度慢的问题

[返回目录](#目录)


#### <a name="其他工具类C_CPP"></a>其他工具类
- :tw-1f195: [lua](https://gitee.com/openharmony-sig/lua) - Lua是一种功能强大、高效、轻量级、可嵌入的脚本语言
- :tw-1f195: [inotify-tools](https://gitee.com/hihopeorg/inotify-tools) - 异步文件系统监控组件，它满足各种各样的文件监控需要，可以监控文件系统的访问属性、读写属性、权限属性、删除创建、移动等操作
- :tw-1f195: [libharu](https://gitee.com/openharmony-sig/libharu) - 用于生成 PDF格式的文件
- :tw-1f195: [leveldb](https://gitee.com/openharmony-sig/leveldb) - 快速键值存储库，提供从字符串键到字符串值的有序映射

[返回目录](#目录)
